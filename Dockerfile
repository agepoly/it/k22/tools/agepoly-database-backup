FROM alpine:3.19.0

WORKDIR /opt/backup/
ENV PATH="$PATH:/opt/backup/"

COPY scripts/* .
RUN chmod +x ./*.sh && \
  apk update && \
  apk add --no-cache postgresql-client mariadb-client aws-cli curl


