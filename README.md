# AGEPoly Database Backup

TBA : usage, disaster recovery

### Credits

Adapted from
- (eeshugerman/postgres-backup-s3)[https://github.com/eeshugerman/postgres-backup-s3] / [MIT License](https://github.com/eeshugerman/postgres-backup-s3/blob/aca41c2af4a3633557af29a2a5d2a988f6211972/LICENSE.txt)

Other references
- https://stackoverflow.com/questions/3669121/dump-all-mysql-tables-into-separate-files-automatically
- http://sprunge.us/fBDL?bash
- https://dba.stackexchange.com/questions/279898/postgresql-backup-all-databases-separate-files