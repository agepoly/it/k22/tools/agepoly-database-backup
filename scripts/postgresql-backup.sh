#! /bin/sh

set -eu
set -o pipefail

# ENV vars renaming
if [ -z "$S3_ENDPOINT" ]; then
	aws_args=""
else
	aws_args="--endpoint-url $S3_ENDPOINT"
fi
if [ -n "$S3_ACCESS_KEY_ID" ]; then
	export AWS_ACCESS_KEY_ID=$S3_ACCESS_KEY_ID
fi
if [ -n "$S3_SECRET_ACCESS_KEY" ]; then
	export AWS_SECRET_ACCESS_KEY=$S3_SECRET_ACCESS_KEY
fi
export PGPASSWORD=$POSTGRES_PASSWORD

echo "Creating backup of $POSTGRES_DATABASE database..."
pg_dump --format=custom \
	-h $POSTGRES_HOST \
	-U $POSTGRES_USER \
	-d $POSTGRES_DATABASE \
	>db.dump
local_file="db.dump"

timestamp=$(date +"%Y-%m-%dT%H:%M:%S")
s3_uri="s3://${S3_BUCKET}/${S3_PREFIX}/${POSTGRES_DATABASE}_${timestamp}.dump"

echo "Uploading backup to $S3_BUCKET..."
aws $aws_args s3 cp "$local_file" "$s3_uri"
rm "$local_file"

echo "Backup complete."

# if [ -n "$BACKUP_KEEP_DAYS" ]; then
# 	sec=$((86400 * BACKUP_KEEP_DAYS))
# 	date_from_remove=$(date -d "@$(($(date +%s) - sec))" +%Y-%m-%d)
# 	backups_query="Contents[?LastModified<='${date_from_remove} 00:00:00'].{Key: Key}"
#
# 	echo "Removing old backups from $S3_BUCKET..."
# 	aws $aws_args s3api list-objects \
# 		--bucket "${S3_BUCKET}" \
# 		--prefix "${S3_PREFIX}" \
# 		--query "${backups_query}" \
# 		--output text |
# 		xargs -n1 -t -I 'KEY' aws $aws_args s3 rm s3://"${S3_BUCKET}"/'KEY'
# 	echo "Removal complete."
# fi
#
